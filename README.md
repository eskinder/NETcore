[![.NET](https://github.com/eskinderg/NETcore/actions/workflows/CI.yml/badge.svg)](https://github.com/eskinderg/NETcore/actions/workflows/CI.yml)

# .NET 6.0
.NET 6.0 API reference application using modern, enterprise-level architecture
